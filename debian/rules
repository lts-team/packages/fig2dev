#!/usr/bin/make -f
#
# (C) 1999-2018 Roland Rosenfeld <roland@debian.org>
#
# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1
#export DH_OPTIONS=-v

DEBDIR=`pwd`/debian/fig2dev

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export FIG2DEV_RGBFILE = $(shell pwd)/fig2dev/rgb.txt

%:
	dh $@

override_dh_auto_configure:
#	backup version.m4:
	if ! test -e version.m4.dh-orig; then \
		cp -f version.m4 version.m4.dh-orig; \
	fi

	dh_auto_configure -- --enable-transfig
# 	rebuild testsuite:
	(cd fig2dev/tests; rm -f testsuite; make testsuite)

override_dh_auto_build:
	dh_auto_build

	(cd transfig/doc; \
	 ../../fig2dev/fig2dev -L latex trans.fig > trans.tex; \
	 latex manual; \
	 latex manual; \
	 dvips -o manual.ps manual.dvi; \
	 ps2pdf manual.ps)

#	uudecode fig2mpdf documentation:
	(cd fig2mpdf/doc && perl uudecode *.uue)
	(cd fig2mpdf/doc && $(MAKE))

# 	strip /ID from sample-presentation.pdf because this is includes current
#	directory and makes package non reproducible
	(cd fig2mpdf/doc \
	 && grep -av '^/ID \[\(<[0-9A-F]\{32\}>\) \1]$$' sample-presentation.pdf \
		> sample-presentation.pdf_without_id \
	 && mv -f sample-presentation.pdf_without_id sample-presentation.pdf)

override_dh_clean:
	dh_clean

	rm -f transfig/doc/trans.tex transfig/doc/*.aux transfig/doc/*.log \
		transfig/doc/*.dvi transfig/doc/*.ps transfig/doc/*.pdf
	rm -f fig2mpdf/doc/*.gif fig2mpdf/doc/*.pdf fig2mpdf/doc/*.jpg
	[ ! -f fig2mpdf/doc/Makefile ] || (cd fig2mpdf/doc && $(MAKE) clean)

#	restore version.m4:
	if test -e version.m4.dh-orig; then \
		mv -f version.m4.dh-orig version.m4; \
	fi

override_dh_auto_install:
	dh_auto_install --destdir=$(DEBDIR)

override_dh_installchangelogs:
	dh_installchangelogs CHANGES

override_dh_compress:
	dh_compress -X.pdf
