From: Thomas Loimer <thomas.loimer@tuwien.ac.at>
Date: Fri Jul 26 23:25:50 2019 +0200
Bug-Debian: https://bugs.debian.org/933075
Bug: https://sourceforge.net/p/mcj/tickets/52/
Origin: https://sourceforge.net/p/mcj/fig2dev/ci/03ea45
Subject: Allow circle arrowheads when mag >= 42, ticket #52
 Circle and half-circle arrowheads would be drawn with 40 + mag/4 points by
 calc_arrow() in bound.c. However, the point arrays passed to calc_arrow()
 would only contain 50 points. With a magnification >= 42, a buffer overrun
 would occur. Simply use 40 points, independent of magnification.
 .
 This fixes CVE-2019-14275

--- a/fig2dev/bound.c
+++ b/fig2dev/bound.c
@@ -905,7 +905,7 @@ calc_arrow(int x1, int y1, int x2, int y
 	    /*
 	     * CIRCLE and HALF-CIRCLE arrowheads
 	     *
-	     * We approximate circles with (40+zoom)/4 points
+	     * We approximate circles with 40 points
 	     */
 	    double	maxx;
 	    double	fix_x, fix_y, xs, ys;
@@ -936,8 +936,8 @@ calc_arrow(int x1, int y1, int x2, int y
 	    dy = my - ys;
 	    fix_x = xs + (dx / 2.0);
 	    fix_y = ys + (dy / 2.0);
-	    /* choose number of points for circle - 40+mag/4 points */
-	    *npoints = np = round(mag/4.0) + 40;
+	    /* choose number of points for circle */
+	    *npoints = np = 40;
 
 	    if (type == 5) {
 		/* full circle */
--- a/fig2dev/tests/read.at
+++ b/fig2dev/tests/read.at
@@ -208,6 +208,29 @@ EOF
 ],1,ignore,ignore)
 AT_CLEANUP
 
+AT_SETUP([process circle arrow at mag > 42, ticket #52])
+AT_KEYWORDS([bound.c])
+# With the -fsanitize=address option, a magnification >= 42 triggers an error.
+# Without the -fsanitize=address option, a segfault only results
+# if the array is accessed far enough from its valid range;
+# Use a magnification of 420.
+AT_CHECK([fig2dev -L box <<EOF
+#FIG 3.2
+Landscape
+Center
+Inches
+Letter
+42000.0
+Single
+-2
+1200 2
+2 1 0 1 0 7 50 -1 -1 0.0 0 0 -1 1 0 2
+	5 0 1.0 60.0 120.0
+	 0 0 600 0
+EOF
+], 0, ignore)
+AT_CLEANUP
+
 AT_BANNER([Dynamically allocate picture file name.])
 
 AT_SETUP([prepend fig file path to picture file name])
--- a/fig2dev/tests/testsuite
+++ b/fig2dev/tests/testsuite
@@ -631,30 +631,31 @@ at_help_all="1;input.at:25;do not overwr
 23;read.at:148;survive debian bugs #881143, #881144;font pic tikz;
 24;read.at:177;reject out-of-range pattern, debian bug #881396;read.c;
 25;read.at:187;survive debian bug #890016;read1_3.c;
-26;read.at:213;prepend fig file path to picture file name;read.c picfilename;
-27;output.at:24;compare arrow tips with template;eps arrows;
-28;output.at:50;honor SOURCE_DATE_EPOCH environment variable;eps creationdate source_date_epoch;
-29;output.at:57;fail on text in SOURCE_DATE_EPOCH;eps creationdate source_date_epoch;
-30;output.at:107;ignore -G option;pict2e options;
-31;output.at:119;compare patterns with template;svg pattern creationdate;
-32;output.at:152;compare fills with arrows to template;svg arrows pattern fill creationdate;
-33;output.at:161;conditionally allocate \\XFigu;tikz newdimen;
-34;output.at:183;pattern with stroke color equal to fill color;tikz pattern;
-35;bitmaps.at:25;gif;bitmaps gif;
-36;bitmaps.at:33;jpeg;bitmaps jpeg jpg;
-37;bitmaps.at:41;pcx;bitmaps pcx;
-38;bitmaps.at:49;png;bitmaps png;
-39;bitmaps.at:57;png with smoothing;bitmaps png smoothing;
-40;bitmaps.at:65;ppm;bitmaps ppm;
-41;bitmaps.at:73;tiff;bitmaps tiff tif;
-42;bitmaps.at:81;xbm;bitmaps xbm;
-43;bitmaps.at:91;xbm with smoothing;bitmaps xbm smoothing;
-44;bitmaps.at:101;xpm;bitmaps xpm;
-45;bitmaps.at:115;eps with acscii preview;bitmaps tmpfile eps-ascii;
-46;bitmaps.at:123;eps with tiff preview;bitmaps tmpfile eps-tiff;
-47;bitmaps.at:132;epsi;embed epsi;
-48;bitmaps.at:144;postscript, created by fig2dev;embed ps;
-49;bitmaps.at:191;postscript, created by latex;embed ps;
+26;read.at:211;process circle arrow at mag > 42, ticket #52;bound.c;
+27;read.at:236;prepend fig file path to picture file name;read.c picfilename;
+28;output.at:24;compare arrow tips with template;eps arrows;
+29;output.at:50;honor SOURCE_DATE_EPOCH environment variable;eps creationdate source_date_epoch;
+30;output.at:57;fail on text in SOURCE_DATE_EPOCH;eps creationdate source_date_epoch;
+31;output.at:107;ignore -G option;pict2e options;
+32;output.at:119;compare patterns with template;svg pattern creationdate;
+33;output.at:152;compare fills with arrows to template;svg arrows pattern fill creationdate;
+34;output.at:161;conditionally allocate \\XFigu;tikz newdimen;
+35;output.at:183;pattern with stroke color equal to fill color;tikz pattern;
+36;bitmaps.at:25;gif;bitmaps gif;
+37;bitmaps.at:33;jpeg;bitmaps jpeg jpg;
+38;bitmaps.at:41;pcx;bitmaps pcx;
+39;bitmaps.at:49;png;bitmaps png;
+40;bitmaps.at:57;png with smoothing;bitmaps png smoothing;
+41;bitmaps.at:65;ppm;bitmaps ppm;
+42;bitmaps.at:73;tiff;bitmaps tiff tif;
+43;bitmaps.at:81;xbm;bitmaps xbm;
+44;bitmaps.at:91;xbm with smoothing;bitmaps xbm smoothing;
+45;bitmaps.at:101;xpm;bitmaps xpm;
+46;bitmaps.at:115;eps with acscii preview;bitmaps tmpfile eps-ascii;
+47;bitmaps.at:123;eps with tiff preview;bitmaps tmpfile eps-tiff;
+48;bitmaps.at:132;epsi;embed epsi;
+49;bitmaps.at:144;postscript, created by fig2dev;embed ps;
+50;bitmaps.at:191;postscript, created by latex;embed ps;
 "
 # List of the all the test groups.
 at_groups_all=`$as_echo "$at_help_all" | sed 's/;.*//'`
@@ -668,7 +669,7 @@ at_fn_validate_ranges ()
   for at_grp
   do
     eval at_value=\$$at_grp
-    if test $at_value -lt 1 || test $at_value -gt 49; then
+    if test $at_value -lt 1 || test $at_value -gt 50; then
       $as_echo "invalid test group: $at_value" >&2
       exit 1
     fi
@@ -1056,29 +1057,29 @@ at_banner_text_4="Find and decompress em
 # Banner 5. read.at:20
 # Category starts at test group 12.
 at_banner_text_5="Sanitize input files in read.c."
-# Banner 6. read.at:211
-# Category starts at test group 26.
+# Banner 6. read.at:234
+# Category starts at test group 27.
 at_banner_text_6="Dynamically allocate picture file name."
 # Banner 7. output.at:22
-# Category starts at test group 27.
+# Category starts at test group 28.
 at_banner_text_7="Test PostScript output language."
 # Banner 8. output.at:66
-# Category starts at test group 30.
+# Category starts at test group 31.
 at_banner_text_8="Test pict2e output language."
 # Banner 9. output.at:118
-# Category starts at test group 31.
+# Category starts at test group 32.
 at_banner_text_9="Test svg output language."
 # Banner 10. output.at:159
-# Category starts at test group 33.
+# Category starts at test group 34.
 at_banner_text_10="Test tikz output language."
 # Banner 11. bitmaps.at:20
-# Category starts at test group 35.
+# Category starts at test group 36.
 at_banner_text_11="Create and embed bitmaps in fig-file."
 # Banner 12. bitmaps.at:111
-# Category starts at test group 45.
+# Category starts at test group 46.
 at_banner_text_12="Creation of temporary files."
 # Banner 13. bitmaps.at:130
-# Category starts at test group 47.
+# Category starts at test group 48.
 at_banner_text_13="Embed postscript variants."
 
 # Take any -C into account.
@@ -3344,19 +3345,78 @@ $at_traceon; }
 read at_status <"$at_status_file"
 #AT_STOP_25
 #AT_START_26
-at_fn_group_banner 26 'read.at:213' \
-  "prepend fig file path to picture file name" "     " 6
+at_fn_group_banner 26 'read.at:211' \
+  "process circle arrow at mag > 42, ticket #52" "   " 5
 at_xfail=no
 (
   $as_echo "26. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
+# With the -fsanitize=address option, a magnification >= 42 triggers an error.
+# Without the -fsanitize=address option, a segfault only results
+# if the array is accessed far enough from its valid range;
+# Use a magnification of 420.
 { set +x
-$as_echo "$at_srcdir/read.at:215: fig2dev -L eps \$srcdir/data/boxwimg.fig | \\
+$as_echo "$at_srcdir/read.at:217: fig2dev -L box <<EOF
+#FIG 3.2
+Landscape
+Center
+Inches
+Letter
+42000.0
+Single
+-2
+1200 2
+2 1 0 1 0 7 50 -1 -1 0.0 0 0 -1 1 0 2
+	5 0 1.0 60.0 120.0
+	 0 0 600 0
+EOF
+"
+at_fn_check_prepare_notrace 'an embedded newline' "read.at:217"
+( $at_check_trace; fig2dev -L box <<EOF
+#FIG 3.2
+Landscape
+Center
+Inches
+Letter
+42000.0
+Single
+-2
+1200 2
+2 1 0 1 0 7 50 -1 -1 0.0 0 0 -1 1 0 2
+	5 0 1.0 60.0 120.0
+	 0 0 600 0
+EOF
+
+) >>"$at_stdout" 2>>"$at_stderr" 5>&-
+at_status=$? at_failed=false
+$at_check_filter
+at_fn_diff_devnull "$at_stderr" || at_failed=:
+echo stdout:; cat "$at_stdout"
+at_fn_check_status 0 $at_status "$at_srcdir/read.at:217"
+$at_failed && at_fn_log_failure
+$at_traceon; }
+
+  set +x
+  $at_times_p && times >"$at_times_file"
+) 5>&1 2>&1 7>&- | eval $at_tee_pipe
+read at_status <"$at_status_file"
+#AT_STOP_26
+#AT_START_27
+at_fn_group_banner 27 'read.at:236' \
+  "prepend fig file path to picture file name" "     " 6
+at_xfail=no
+(
+  $as_echo "27. $at_setup_line: testing $at_desc ..."
+  $at_traceon
+
+
+{ set +x
+$as_echo "$at_srcdir/read.at:238: fig2dev -L eps \$srcdir/data/boxwimg.fig | \\
 	\$FGREP \"% Begin Imported EPS File: \$srcdir/data/line.eps\"
 "
-at_fn_check_prepare_notrace 'an embedded newline' "read.at:215"
+at_fn_check_prepare_notrace 'an embedded newline' "read.at:238"
 ( $at_check_trace; fig2dev -L eps $srcdir/data/boxwimg.fig | \
 	$FGREP "% Begin Imported EPS File: $srcdir/data/line.eps"
 
@@ -3365,7 +3425,7 @@ at_status=$? at_failed=false
 $at_check_filter
 at_fn_diff_devnull "$at_stderr" || at_failed=:
 echo stdout:; cat "$at_stdout"
-at_fn_check_status 0 $at_status "$at_srcdir/read.at:215"
+at_fn_check_status 0 $at_status "$at_srcdir/read.at:238"
 $at_failed && at_fn_log_failure
 $at_traceon; }
 
@@ -3373,13 +3433,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_26
-#AT_START_27
-at_fn_group_banner 27 'output.at:24' \
+#AT_STOP_27
+#AT_START_28
+at_fn_group_banner 28 'output.at:24' \
   "compare arrow tips with template" "               " 7
 at_xfail=no
 (
-  $as_echo "27. $at_setup_line: testing $at_desc ..."
+  $as_echo "28. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3425,13 +3485,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_27
-#AT_START_28
-at_fn_group_banner 28 'output.at:50' \
+#AT_STOP_28
+#AT_START_29
+at_fn_group_banner 29 'output.at:50' \
   "honor SOURCE_DATE_EPOCH environment variable" "   " 7
 at_xfail=no
 (
-  $as_echo "28. $at_setup_line: testing $at_desc ..."
+  $as_echo "29. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3456,13 +3516,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_28
-#AT_START_29
-at_fn_group_banner 29 'output.at:57' \
+#AT_STOP_29
+#AT_START_30
+at_fn_group_banner 30 'output.at:57' \
   "fail on text in SOURCE_DATE_EPOCH" "              " 7
 at_xfail=no
 (
-  $as_echo "29. $at_setup_line: testing $at_desc ..."
+  $as_echo "30. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3487,13 +3547,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_29
-#AT_START_30
-at_fn_group_banner 30 'output.at:107' \
+#AT_STOP_30
+#AT_START_31
+at_fn_group_banner 31 'output.at:107' \
   "ignore -G option" "                               " 8
 at_xfail=no
 (
-  $as_echo "30. $at_setup_line: testing $at_desc ..."
+  $as_echo "31. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3530,13 +3590,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_30
-#AT_START_31
-at_fn_group_banner 31 'output.at:119' \
+#AT_STOP_31
+#AT_START_32
+at_fn_group_banner 32 'output.at:119' \
   "compare patterns with template" "                 " 9
 at_xfail=no
 (
-  $as_echo "31. $at_setup_line: testing $at_desc ..."
+  $as_echo "32. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3562,13 +3622,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_31
-#AT_START_32
-at_fn_group_banner 32 'output.at:152' \
+#AT_STOP_32
+#AT_START_33
+at_fn_group_banner 33 'output.at:152' \
   "compare fills with arrows to template" "          " 9
 at_xfail=no
 (
-  $as_echo "32. $at_setup_line: testing $at_desc ..."
+  $as_echo "33. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3591,13 +3651,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_32
-#AT_START_33
-at_fn_group_banner 33 'output.at:161' \
+#AT_STOP_33
+#AT_START_34
+at_fn_group_banner 34 'output.at:161' \
   "conditionally allocate \\XFigu" "                  " 10
 at_xfail=no
 (
-  $as_echo "33. $at_setup_line: testing $at_desc ..."
+  $as_echo "34. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3656,13 +3716,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_33
-#AT_START_34
-at_fn_group_banner 34 'output.at:183' \
+#AT_STOP_34
+#AT_START_35
+at_fn_group_banner 35 'output.at:183' \
   "pattern with stroke color equal to fill color" "  " 10
 at_xfail=no
 (
-  $as_echo "34. $at_setup_line: testing $at_desc ..."
+  $as_echo "35. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3701,13 +3761,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_34
-#AT_START_35
-at_fn_group_banner 35 'bitmaps.at:25' \
+#AT_STOP_35
+#AT_START_36
+at_fn_group_banner 36 'bitmaps.at:25' \
   "gif" "                                            " 11
 at_xfail=no
 (
-  $as_echo "35. $at_setup_line: testing $at_desc ..."
+  $as_echo "36. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3735,13 +3795,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_35
-#AT_START_36
-at_fn_group_banner 36 'bitmaps.at:33' \
+#AT_STOP_36
+#AT_START_37
+at_fn_group_banner 37 'bitmaps.at:33' \
   "jpeg" "                                           " 11
 at_xfail=no
 (
-  $as_echo "36. $at_setup_line: testing $at_desc ..."
+  $as_echo "37. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3769,13 +3829,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_36
-#AT_START_37
-at_fn_group_banner 37 'bitmaps.at:41' \
+#AT_STOP_37
+#AT_START_38
+at_fn_group_banner 38 'bitmaps.at:41' \
   "pcx" "                                            " 11
 at_xfail=no
 (
-  $as_echo "37. $at_setup_line: testing $at_desc ..."
+  $as_echo "38. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3803,13 +3863,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_37
-#AT_START_38
-at_fn_group_banner 38 'bitmaps.at:49' \
+#AT_STOP_38
+#AT_START_39
+at_fn_group_banner 39 'bitmaps.at:49' \
   "png" "                                            " 11
 at_xfail=no
 (
-  $as_echo "38. $at_setup_line: testing $at_desc ..."
+  $as_echo "39. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3837,13 +3897,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_38
-#AT_START_39
-at_fn_group_banner 39 'bitmaps.at:57' \
+#AT_STOP_39
+#AT_START_40
+at_fn_group_banner 40 'bitmaps.at:57' \
   "png with smoothing" "                             " 11
 at_xfail=no
 (
-  $as_echo "39. $at_setup_line: testing $at_desc ..."
+  $as_echo "40. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3871,13 +3931,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_39
-#AT_START_40
-at_fn_group_banner 40 'bitmaps.at:65' \
+#AT_STOP_40
+#AT_START_41
+at_fn_group_banner 41 'bitmaps.at:65' \
   "ppm" "                                            " 11
 at_xfail=no
 (
-  $as_echo "40. $at_setup_line: testing $at_desc ..."
+  $as_echo "41. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3905,13 +3965,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_40
-#AT_START_41
-at_fn_group_banner 41 'bitmaps.at:73' \
+#AT_STOP_41
+#AT_START_42
+at_fn_group_banner 42 'bitmaps.at:73' \
   "tiff" "                                           " 11
 at_xfail=no
 (
-  $as_echo "41. $at_setup_line: testing $at_desc ..."
+  $as_echo "42. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3939,13 +3999,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_41
-#AT_START_42
-at_fn_group_banner 42 'bitmaps.at:81' \
+#AT_STOP_42
+#AT_START_43
+at_fn_group_banner 43 'bitmaps.at:81' \
   "xbm" "                                            " 11
 at_xfail=no
 (
-  $as_echo "42. $at_setup_line: testing $at_desc ..."
+  $as_echo "43. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -3975,13 +4035,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_42
-#AT_START_43
-at_fn_group_banner 43 'bitmaps.at:91' \
+#AT_STOP_43
+#AT_START_44
+at_fn_group_banner 44 'bitmaps.at:91' \
   "xbm with smoothing" "                             " 11
 at_xfail=no
 (
-  $as_echo "43. $at_setup_line: testing $at_desc ..."
+  $as_echo "44. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -4011,13 +4071,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_43
-#AT_START_44
-at_fn_group_banner 44 'bitmaps.at:101' \
+#AT_STOP_44
+#AT_START_45
+at_fn_group_banner 45 'bitmaps.at:101' \
   "xpm" "                                            " 11
 at_xfail=no
 (
-  $as_echo "44. $at_setup_line: testing $at_desc ..."
+  $as_echo "45. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -4049,13 +4109,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_44
-#AT_START_45
-at_fn_group_banner 45 'bitmaps.at:115' \
+#AT_STOP_45
+#AT_START_46
+at_fn_group_banner 46 'bitmaps.at:115' \
   "eps with acscii preview" "                        " 12
 at_xfail=no
 (
-  $as_echo "45. $at_setup_line: testing $at_desc ..."
+  $as_echo "46. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -4083,13 +4143,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_45
-#AT_START_46
-at_fn_group_banner 46 'bitmaps.at:123' \
+#AT_STOP_46
+#AT_START_47
+at_fn_group_banner 47 'bitmaps.at:123' \
   "eps with tiff preview" "                          " 12
 at_xfail=no
 (
-  $as_echo "46. $at_setup_line: testing $at_desc ..."
+  $as_echo "47. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -4115,13 +4175,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_46
-#AT_START_47
-at_fn_group_banner 47 'bitmaps.at:132' \
+#AT_STOP_47
+#AT_START_48
+at_fn_group_banner 48 'bitmaps.at:132' \
   "epsi" "                                           " 13
 at_xfail=no
 (
-  $as_echo "47. $at_setup_line: testing $at_desc ..."
+  $as_echo "48. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -4151,13 +4211,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_47
-#AT_START_48
-at_fn_group_banner 48 'bitmaps.at:144' \
+#AT_STOP_48
+#AT_START_49
+at_fn_group_banner 49 'bitmaps.at:144' \
   "postscript, created by fig2dev" "                 " 13
 at_xfail=no
 (
-  $as_echo "48. $at_setup_line: testing $at_desc ..."
+  $as_echo "49. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -4232,13 +4292,13 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_48
-#AT_START_49
-at_fn_group_banner 49 'bitmaps.at:191' \
+#AT_STOP_49
+#AT_START_50
+at_fn_group_banner 50 'bitmaps.at:191' \
   "postscript, created by latex" "                   " 13
 at_xfail=no
 (
-  $as_echo "49. $at_setup_line: testing $at_desc ..."
+  $as_echo "50. $at_setup_line: testing $at_desc ..."
   $at_traceon
 
 
@@ -4314,4 +4374,4 @@ $at_traceon; }
   $at_times_p && times >"$at_times_file"
 ) 5>&1 2>&1 7>&- | eval $at_tee_pipe
 read at_status <"$at_status_file"
-#AT_STOP_49
+#AT_STOP_50
